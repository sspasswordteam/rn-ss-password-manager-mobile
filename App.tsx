import { StatusBar } from 'expo-status-bar';
import * as FileSystem from 'expo-file-system';
import React, { useState } from 'react';
import { TouchableHighlight, View, StyleSheet, Text, KeyboardAvoidingView, FlatList } from 'react-native';
import { Header } from 'react-native-elements';
import * as DocumentPicker from 'expo-document-picker';
import Dialog from 'react-native-dialog';
import crypto from 'crypto-es';
import * as Clipboard from 'expo-clipboard';
import { DocumentResult } from 'expo-document-picker';

export type Props = { };

export type Password = {
  description:string;
};

const App: React.FC<Props> = () => {
  const [algorithm, setAlgorithm] = useState("aes-256-ctr");
  const [indexURI, setIndexURI] = useState("");
  const [isDialogVisible, setIsDialogVisible] = useState(false);
  const [passwords, setPasswords] = useState([] as Password[]);
  const [password, setPassword] = useState("");

  const selectFile = () => {
    
    // iPhone/Android
    DocumentPicker.getDocumentAsync({
      type: ['public.data']
    }).then((res:DocumentResult) => {
      if (res.type == 'cancel')
      {
        // User cancelled the picker, exit any dialogs or menus and move on
      }
      else
      {
        setIsDialogVisible(true);
        setIndexURI(res.uri);
      }
    }, (err: Error) => {
      alert(err);
    });
  }

  const loadPasswords = () => {

    setIsDialogVisible(false);
    FileSystem.readAsStringAsync(indexURI.substring(7)).then((contents:string) => {

      var binaryData = Buffer.from(contents, 'base64');
      var deciphered = crypto.AES.decrypt(binaryData.toString(), password, { mode: crypto.mode.CTR });
      var json = deciphered.toString();
      var arr:Password[] = [];
      try {
        arr = JSON.parse(json);
      } catch(e) {
        alert('Password errata');
      }
      setPasswords(Object.values(arr));
    }, (error:Error) => { alert(error) });
  }

  const onPress = (item:any) => {
    alert(item.username + " - " + item.password);
    Clipboard.setString(item.password);
  }

  return (
    <KeyboardAvoidingView style={styles.container} behavior="height" enabled>
        <StatusBar />
        <Header
          centerComponent={{ text: 'SS Password Manager', style: { color: '#fff' } }}
          rightComponent={{ icon: 'settings', color: '#fff', onPress:()=>{selectFile()}  }}
        />
        <Dialog.Container visible={isDialogVisible}>
          <Dialog.Title>Autenticazione</Dialog.Title>
          <Dialog.Description>
          Inserisci la master password
          </Dialog.Description>
          <Dialog.Input textInputRef={ password } />
          <Dialog.Button label="Annulla" onPress={ () => {setIsDialogVisible(false)} } />
          <Dialog.Button label="Sblocca" onPress={ () => { loadPasswords() }} />
        </Dialog.Container>
        <FlatList
          style={{flex:1}}
          data={passwords}
          renderItem={({item}) => <View style={{flex:1, borderBottomWidth:1, borderBottomColor:'grey'}}><TouchableHighlight onPress={() => { onPress(item) }}><Text style={{padding: 20}}>{item.description}</Text></TouchableHighlight></View>}>
        </FlatList>
      </KeyboardAvoidingView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  }
});

export default App;